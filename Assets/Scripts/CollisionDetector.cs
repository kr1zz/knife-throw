using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetector : MonoBehaviour
{
    public Action OnMissed;
    public Action OnHit;
    public Action OnCollected;
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Obstacle") )
        {
            OnMissed?.Invoke();
            
        }
        if (other.gameObject.CompareTag("Circle"))
        {
            OnHit?.Invoke();
        }
        
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Apple"))
        {
            other.GetComponent<Apple>().DestroyApple();
            
        }
        
    }
}
