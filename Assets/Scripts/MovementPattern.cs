using System;
using System.Collections;
using System.Collections.Generic;
using Managers;
using UnityEngine;

public class MovementPattern : MonoBehaviour
{
    [SerializeField] private List<CircleMovement> circleMovements;

    private CircleMovement _currentCircleMovement;
    
    private int _currentCircleMovementIndex = 0;
    private Vector3 _direction;
    private float _startRotationSpeed;
    private float _rotationSpeed;
    private float _angle;
    private float _currentAngle;
    private int _repeatTimes = 1;
    private float _acceleration;

    

    private void OnEnable()
    {
        Initialize();
    }
    

    private void Initialize()
    {
        _currentCircleMovement = circleMovements[_currentCircleMovementIndex];
        DetermineDirection();
        _startRotationSpeed = _currentCircleMovement.RotationSpeed;
        _rotationSpeed = _startRotationSpeed;

        _angle = _currentCircleMovement.Angle;
        _repeatTimes = _currentCircleMovement.RepeatTimes;
        _acceleration = _currentCircleMovement.Acceleration;
        StartCoroutine(Movement());
    }

    private IEnumerator Movement()
    {
        if (_currentCircleMovement.IsShaking)
        {
            while (_angle > Mathf.Abs(_currentAngle))
            {
                transform.Rotate(Vector3.forward, _rotationSpeed, Space.World);
                _currentAngle += _rotationSpeed; 
                yield return new WaitForFixedUpdate();
            }
            _currentAngle = 0f;
            while (_angle > Mathf.Abs(_currentAngle))
            {
                transform.Rotate(Vector3.forward, _rotationSpeed * -1, Space.World);
                _currentAngle += _rotationSpeed;
                yield return new WaitForFixedUpdate();

            }
        }
        else
        {
            
            while (_angle > Mathf.Abs(_currentAngle))
            {
                transform.Rotate(Vector3.forward, _rotationSpeed * _direction.x, Space.World);
                if (_angle/2 > Mathf.Abs(_currentAngle))
                { 
                    _rotationSpeed += _acceleration;
                }
                else 
                {
                    _rotationSpeed -= _acceleration;



                }
                _currentAngle += _rotationSpeed;
                yield return new WaitForFixedUpdate();

            }
            
            

        }
        
        _rotationSpeed = _currentCircleMovement.RotationSpeed;
        _currentAngle = 0f;
        _repeatTimes--;
        
        if (_repeatTimes == 0)
        {
            if (_currentCircleMovementIndex < circleMovements.Count - 1)
            {
                _currentCircleMovementIndex++;

            }
            else
            {
                _currentCircleMovementIndex = 0;
            }

            _currentAngle = 0f;
            Initialize();
        }
        else
        {
            StartCoroutine(Movement());
        }
        
        
    }

    

    private void DetermineDirection()
    {
        switch (_currentCircleMovement.direction)
        {
            case CircleMovement.Direction.Left:
                _direction = Vector3.left;
                break;
            case CircleMovement.Direction.Right:
                _direction = Vector3.right;
                break;
            default:
                break;
        }
    }
    
    
    
    
}
