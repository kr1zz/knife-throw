using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Levels Progression")]
public class LevelProgression : ScriptableObject
{
    [SerializeField] private List<LevelStages> levelPrefabs;
    [SerializeField] private BossStages bossStages;
    [SerializeField] private int lastLevel = 0;


    public int LastLevel
    {
        get => lastLevel;
        set
        {
            if (value < LevelCount)
            {
                lastLevel = value;
            }
            else
            {
                lastLevel = -1;
            }
        }
    }

    public BossStages BossStages => bossStages;

    private int LevelCount => levelPrefabs.Count;
    public LevelStages GetLevel(int index)
    {
        return levelPrefabs[index];
    }


}
