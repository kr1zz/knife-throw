using System.Collections.Generic;
using UnityEngine;

namespace Level
{
    public class StageConfig : MonoBehaviour
    {
        [SerializeField] private int knivesAmount;
    
        [SerializeField] private List<float> applesAngles = new List<float>();
        [SerializeField] private List<float> obstaclesAngles = new List<float>();

    
        public int KnivesAmount => knivesAmount;
    
        public List<float> ApplesAngles => applesAngles;

        public List<float> ObstaclesAngles => obstaclesAngles;
        
    }
}
