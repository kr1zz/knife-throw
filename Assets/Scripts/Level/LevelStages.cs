using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "LevelStages")]

public class LevelStages : ScriptableObject
{
    [SerializeField] private List<GameObject> stagePrefabs;
    [SerializeField] private Material backgroundMaterial;
    private int lastStage = 0;

    public Material BackgroundMaterial => backgroundMaterial;

    private void OnEnable()
    {
        LastStage = 0;
    }

    public int LastStage
    {
        get => lastStage;
        set
        {
            if (value < StageCount)
            {
                lastStage = value;
            }
            else
            {
                lastStage = -1;
            }
           
        }
    }
    
    public int StageCount => stagePrefabs.Count;

    public GameObject GetStage(int index)
    {
        return stagePrefabs[index];
    }
    
    
    
    
}
