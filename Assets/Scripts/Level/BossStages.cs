using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
[CreateAssetMenu(fileName = "Boss Stage")]

public class BossStages : ScriptableObject
{
    [SerializeField] private List<GameObject> commonBossStagePrefabs;
    [SerializeField] private List<GameObject> rareBossStagePrefabs;
    [SerializeField] private float rareChance = 70f;
    [SerializeField] private List<GameObject> legendaryBossStagePrefabs;
    [SerializeField] private float legendaryChance = 90f;
    



    public GameObject GetRandomBoss()
    {
        var range = Random.Range( 0f, 101f );
        if (range > legendaryChance)
        {
            return GetRandomBossInRarity(legendaryBossStagePrefabs);
        }
        if (range > rareChance)
        {
            return GetRandomBossInRarity(rareBossStagePrefabs);
        }
        return GetRandomBossInRarity(commonBossStagePrefabs);
        
    }
    
    
    private GameObject GetRandomBossInRarity(List<GameObject> rarity)
    {
       
        var bossIndex = Random.Range(0, rarity.Count);
        return rarity[bossIndex]; 

    }
    
    
    

}
