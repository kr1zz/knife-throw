using System;
using System.Collections;
using System.Collections.Generic;
using Managers;
using UnityEngine;

public class ScoreCounter : MonoBehaviour
{
    private int _currentScore;
    private int _finalScore;

    public int CurrentScore => _currentScore;

    public int FinalScore => _finalScore;

    public Action<int> OnScoreChanged;

    public void IncreaseCurrentScore()
    {
        _currentScore++;
        OnScoreChanged?.Invoke(_currentScore);
       
    }

    public void ClearScore()
    {
        UIManager.Instance.SetFinalScore(_currentScore);
        _currentScore = 0;
        OnScoreChanged?.Invoke(_currentScore);
    }

    

}
