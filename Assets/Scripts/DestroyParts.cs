using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class DestroyParts : MonoBehaviour
{
    [SerializeField] private float maxDirectionX;
    [SerializeField] private float maxDirectionY;
    [SerializeField] private float maxTorque;

    private Rigidbody2D _rigidbody2D;
    private void OnEnable()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        var direction = new Vector2(Random.Range(-maxDirectionX, maxDirectionX),
            Random.Range(-maxDirectionY, maxDirectionY));
        _rigidbody2D.AddForce(direction, ForceMode2D.Impulse);
        _rigidbody2D.AddTorque(Random.Range(-maxTorque, maxTorque), ForceMode2D.Impulse);
    }


    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
