using System;
using System.Collections;
using System.Collections.Generic;
using Managers;
using UnityEngine;
using Random = UnityEngine.Random;

public class Knife : MonoBehaviour
{
    [SerializeField] private string obstacle = "Obstacle";
    [SerializeField] private string flyingKnife = "FlyingKnife";
    [SerializeField] private float throwSpeed = 1f;
    [SerializeField] private float reboundSpeed = 1f;
    [SerializeField] private float maxTorque = 5f;
    private bool _isHit = false;
    private Collider2D _bladeCollider;
    private Collider2D _gripCollider;
    private CollisionDetector _collisionDetector;
    private Rigidbody2D _knifeRigidbody2D;


    public bool IsHit
    {
        set => _isHit = value;
    }

    public string Obstacle => obstacle;
    public string FlyingKnife => flyingKnife;
        

    public CollisionDetector CollisionDetector => _collisionDetector;

    public void Initialize()
    {
        _bladeCollider = gameObject.GetComponentInChildren<Collider2D>();
        _gripCollider = gameObject.transform.Find("Grip").GetComponent<Collider2D>();
        _collisionDetector = gameObject.GetComponentInChildren<CollisionDetector>();
        _knifeRigidbody2D = gameObject.GetComponent<Rigidbody2D>();
        SetTag(flyingKnife);
        gameObject.SetActive(true);
        _gripCollider.enabled = false;
    }

    public void StartMoving()
    {
        _knifeRigidbody2D.constraints = RigidbodyConstraints2D.None;
    }

    private void StopMoving()
    {
        _knifeRigidbody2D.constraints = RigidbodyConstraints2D.FreezeAll;
    }
    
    private void TurnOffBladeCollider()
    {
        _bladeCollider.enabled = false;
    }

    private void SetTag(string tag)
    {
        gameObject.tag = tag;
    }

    public void Throw()
    {
        StartMoving();
        _knifeRigidbody2D.AddForce(Vector2.up * throwSpeed, ForceMode2D.Impulse);
    }
    
    
    public void ReboundKnife()
    {
        _gripCollider.enabled = true;
        _knifeRigidbody2D.Sleep();
        _knifeRigidbody2D.AddForce(Vector2.down * reboundSpeed, ForceMode2D.Impulse);
        _knifeRigidbody2D.AddTorque(Random.Range(-maxTorque, maxTorque), ForceMode2D.Impulse);
        TurnOffBladeCollider();
    }


    public void Stick()
    {
        StopMoving();
        SetTag(Obstacle);
        TurnOffBladeCollider();
        _gripCollider.enabled = true;

    }
    
    
    

    private void OnBecameInvisible()
    {
       
        Destroy(gameObject);
        if (_isHit) return;
        if (CompareTag("Obstacle")) return;
        if (CompareTag("Untagged")) return;
        GameManager.Instance.LoseGame();
        
    }
}
