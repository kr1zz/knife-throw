using System.Collections;
using System.Collections.Generic;
using Managers;
using Unity.Mathematics;
using UnityEngine;

public class Apple : MonoBehaviour
{
   [SerializeField] private GameObject destroyedApplePrefab;
   private GameObject _destroyedApple;
   
   public void DestroyApple()
   {
      Destroy(gameObject);
      GameManager.Instance.IncreaseCollectedApples();
      _destroyedApple = Instantiate(destroyedApplePrefab, gameObject.transform.position, quaternion.identity);
      
   }
   private void OnBecameInvisible()
   {
      Destroy(gameObject);
   }

   
   
}
