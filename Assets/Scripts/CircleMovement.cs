using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(fileName = "Circle Movement")]

public class CircleMovement : ScriptableObject
{
    [SerializeField] private float rotationSpeed = 0.5f;

    [SerializeField] public Direction direction;
    
    [Range(0, 1080)]
    [SerializeField] private float angle = 90f;

    [SerializeField] private bool isShaking = false;
    [SerializeField] private int repeatTimes;
    [SerializeField] private float acceleration = 0f;

    public float RotationSpeed => rotationSpeed;

    public float Angle => angle;
    
    public bool IsShaking => isShaking;

    public float Acceleration => acceleration;

    public int RepeatTimes
    {
        get
        {
            if (repeatTimes > 0)
                return repeatTimes;
            
            throw new InvalidOperationException("RepeatTimes must be greater than 0");
        }
        
    }

    public enum Direction
    {
        Left,
        Right
    }
    
    
    
    
     
    
}
