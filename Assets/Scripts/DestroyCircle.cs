using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class DestroyCircle : MonoBehaviour
{
    [SerializeField] private GameObject destroyedCircle;
    [SerializeField] private float maxDirectionX;
    [SerializeField] private float maxDirectionY;
    [SerializeField] private float maxTorque;

    private GameObject _destroyedCircle;
    private void Start()
    {
        
    }



    public void Destroy()
    {
        Destroy(transform.Find("Sprite").gameObject);
        for (int i = 1; i < transform.childCount; i++)
        {
            var obj = transform.GetChild(i);
            obj.tag = "Untagged";
            var RB = obj.GetComponent<Rigidbody2D>();
            RB.constraints = RigidbodyConstraints2D.None;
            RB.AddTorque(Random.Range(-maxTorque, maxTorque), ForceMode2D.Impulse);
            var direction = new Vector2(Random.Range(-maxDirectionX, maxDirectionX), Random.Range(-maxDirectionX, maxDirectionY));
            RB.AddForce(direction, ForceMode2D.Impulse);
            
        }
        transform.DetachChildren();
        Destroy(gameObject);
        _destroyedCircle = Instantiate(destroyedCircle, gameObject.transform.position, quaternion.identity);
        
        
    }
}
