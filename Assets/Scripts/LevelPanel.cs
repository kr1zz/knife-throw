using System;
using System.Collections;
using System.Collections.Generic;
using Level;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelPanel : MonoBehaviour
{
    [SerializeField] private GameObject prefabKnifeIcon;
    [SerializeField] private GameObject prefabStageIcon;
    [SerializeField] private GameObject prefabBossIcon;
    [SerializeField] private float knifeIconsOffset = 30f;
    [SerializeField] private float stageIconsOffset = 30f;
    [SerializeField] private Color passedStageColor;
    [SerializeField] private float bossIconSpeed = 1f;
    
    private readonly List<GameObject> _knifeIcons = new List<GameObject>();
    private readonly List<GameObject> _stagesIcons = new List<GameObject>();
    private ScoreCounter _scoreCounter;
    private TextMeshProUGUI _stageText;
    private TextMeshProUGUI _scoreText;
    private GameObject _knifeCounter;
    private GameObject _stagesPanel;
    private Color _defaultColor;
    private float _knifeIconSize;
    private float _bossIconSize;
    private GameObject _bossIcon;
    

    private void OnEnable()
    {
        _stageText = transform.Find("Level").GetComponent<TextMeshProUGUI>();
        _defaultColor = _stageText.faceColor;
        _knifeCounter = transform.Find("KnifeCounter").gameObject;
        _stagesPanel = transform.Find("StagesPanel").gameObject;
        _scoreCounter = transform.Find("ScoreCounter").GetComponent<ScoreCounter>();
        _scoreText = _scoreCounter.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        _knifeIconSize = prefabKnifeIcon.GetComponent<RectTransform>().rect.height;
        _bossIconSize = prefabBossIcon.GetComponent<RectTransform>().rect.height;
        _scoreCounter.OnScoreChanged += UpdateScoreText;
        _scoreText.text = "0";
        
    }

    public void InitializeLevelPanel(int stagesCount)
    {
        Unload();
        SetStagePanel(stagesCount);
        
    }

    public void InitializeLevelPanelOnStage(StageConfig config, int currentLevel, int currentStage)
    {
        SetStageText(currentLevel, currentStage);
        DrawKnifeIcons(config.KnivesAmount);
        MarkCurrentStage(currentStage);
    }

    public void InitializeLevelPanelOnStage(BossConfig config, int currentStage)
    {
        SetBossText(config.BossName, config.TextColor);
        DrawKnifeIcons(config.KnivesAmount);
        MarkCurrentStage(currentStage);
    }





    private void SetBossText(string bossName, Color textColor)
    {
        _stageText.text = "Boss: " + bossName;
        _stageText.color = textColor;
    }
    
    
    private void SetStageText(int currentLevel, int currentStage)
    {
        
        _stageText.text = "Level " + ++currentLevel + "." + ++currentStage;
        _stageText.color = _defaultColor;
    }

    



    private void DrawKnifeIcons(int knivesCount)
    {
        var offset = new Vector3(0f , _knifeIconSize + knifeIconsOffset, 0f);
        for (int i = 0; i < knivesCount; i++)
        {
            _knifeIcons.Add(Instantiate(prefabKnifeIcon, _knifeCounter.transform));
            if (i > 0)
            {
                _knifeIcons[i].transform.localPosition = _knifeIcons[i-1].transform.localPosition +  offset;

            }
        }

    }

    private void Unload()
    {
        for (int i = _knifeIcons.Count - 1; i >= 0; i--)
        {
            DestroyKnifeIcon();

        }
        DestroyStageIcons();
        

        if (_knifeIcons.Count != 0)
        {
            Debug.LogWarning("После удаления: " + _knifeIcons.Count);
        }
    }

    private void DestroyStageIcons()
    {
        foreach (var stagesIcon in _stagesIcons)
        {
            Destroy(stagesIcon);
        }
        _stagesIcons.Clear();
    }
    
    public void DestroyKnifeIcon()
    {
        Destroy(_knifeIcons[_knifeIcons.Count - 1]);
        _knifeIcons.Remove(_knifeIcons[_knifeIcons.Count - 1]);
    }

    private void SetPanelSize(int stagesCount)
    {
        var offset = 30f;
        var width = _bossIconSize;
        RectTransform rt = _stagesPanel.GetComponent<RectTransform>();
        Vector2 rectSize = new Vector2(stagesCount * (width + offset) , width); 
        rt.sizeDelta = rectSize;
    }

    private void CreateStageIcons(int stagesCount)
    {
        var offset = new Vector3(stageIconsOffset + _bossIconSize, 0f, 0f);
        for (int i = 0; i < stagesCount - 1 ; i++)
        {
            _stagesIcons.Add(Instantiate(prefabStageIcon, _stagesPanel.transform ));
            if (i > 0)
            {
                _stagesIcons[i].transform.localPosition = _stagesIcons[i - 1].transform.localPosition + offset;
            }
            else _stagesIcons[i].transform.localPosition += offset / 2;
        }
        _stagesIcons.Add(Instantiate(prefabBossIcon, _stagesPanel.transform));
        _bossIcon = _stagesIcons[_stagesIcons.Count - 1];
        _bossIcon.transform.localPosition =
            _stagesIcons[_stagesIcons.Count - 2].transform.localPosition + offset;

    }

    private void MarkCurrentStage(int currentStage)
    {
        if (currentStage != -1)
        {
            _stagesIcons[currentStage].GetComponent<Image>().color = passedStageColor;
        }
        else
        {
            _stagesIcons[_stagesIcons.Count - 1].GetComponent<Image>().color = passedStageColor;
            AnimateBossIcon();
        }
    }

    private void AnimateBossIcon()
    {
        for (int i = _stagesIcons.Count - 2; i >= 0; i--)
        {
            Destroy(_stagesIcons[i]);
        }
        StartCoroutine(MoveBossIcon());
    }

    private IEnumerator MoveBossIcon()
    {
        while (_bossIcon.transform.position.x > Screen.width/2f)
        {
            _bossIcon.transform.Translate(Vector3.left * bossIconSpeed);
            yield return new WaitForFixedUpdate();
        }
    }
    
    private void SetStagePanel(int stageCounts)
    {
        SetPanelSize(stageCounts);
        CreateStageIcons(stageCounts);
    }

    
    
    private void UpdateScoreText(int currentScore)
    {
        _scoreText.text = currentScore.ToString();
    }

   
    
    
}
