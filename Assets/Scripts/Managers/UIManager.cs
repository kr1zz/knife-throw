using System;
using System.Collections;
using System.Collections.Generic;
using Level;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

namespace Managers
{
    public class UIManager : Singleton<UIManager>
    {
        [SerializeField] private GameObject winPanel;
        [SerializeField] private GameObject losePanel;
        [SerializeField] private GameObject mainMenuPanel;
        [SerializeField] private GameObject background;
        [SerializeField] private LevelPanel levelPanel;
        [SerializeField] private GameObject appleCounter;
        
        private TextMeshProUGUI _appleText;



        protected override void Awake()
        {
            base.Awake(); 
            mainMenuPanel.SetActive(true);
            _appleText = appleCounter.GetComponentInChildren<TextMeshProUGUI>();
            HideNewRecord();
           
        }

        private void Start()
        {
            GameManager.Instance.OnRecordBroken += SetNewRecord;
            GameManager.Instance.OnCollected += UpdateAppleText;
            _appleText.text = GameManager.Instance.GetApplesCount().ToString();
            
        }
        
        private void OnDisable()
        {
            GameManager.Instance.OnRecordBroken -= SetNewRecord;
            GameManager.Instance.OnCollected -= UpdateAppleText;

        }

        public void ShowWinPanel()
        {
            winPanel.SetActive(true);
        }
        public void ShowLosePanel()
        {
            losePanel.SetActive(true);
        }

        private void SetBackgroundImage(Material material)
        {
            background.GetComponent<MeshRenderer>().material = material;
        }

        
        public void InitializeLevelUI(Material material, int stagesCount)
        {
            SetBackgroundImage(material);
            levelPanel.InitializeLevelPanel(stagesCount);
            
        }
        
        public void InitializeStageUI(StageConfig config, int currentLevel, int currentStage)
        {
            levelPanel.InitializeLevelPanelOnStage(config, currentLevel, currentStage);
        }

        public void InitializeStageUI(BossConfig config,  int currentStage)
        {
            levelPanel.InitializeLevelPanelOnStage(config, currentStage);

        }

        public void DestroyKnifeIcon()
        {
            levelPanel.DestroyKnifeIcon();
        }


        public void SetFinalScore(int finalScore)
        {
            losePanel.transform.Find("FinalScore").GetComponent<TextMeshProUGUI>().text ="Final score: " + finalScore;
        }

        private void SetNewRecord()
        {
            losePanel.transform.Find("NewRecord").gameObject.SetActive(true);
        }
        
        

        public void HideNewRecord()
        {
            losePanel.transform.Find("NewRecord").gameObject.SetActive(false);

        }
        public void UpdateAppleText(int currentApples)
        {
            _appleText.text = currentApples.ToString();
        }
    }
}
