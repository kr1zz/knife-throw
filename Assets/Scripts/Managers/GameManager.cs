using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using Level;
using Skins;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Managers
{
    public class GameManager : Singleton<GameManager>
    {
        [SerializeField] private LevelProgression levelProgression;
        [SerializeField] private PlayerStats playerStats;
        [SerializeField] private KnifeSpawner knifeSpawner;
        [SerializeField] private ScoreCounter scoreCounter;
        [SerializeField] private Vector3 circlePosition = new Vector3(0, 1, 0);
        [SerializeField] private SkinsMenuController skinsMenuController;
        private LevelStages _currentLevel;
        private GameObject _currentStage;
        private StageConfig _stageConfig;
        private BossConfig _bossConfig;
        private bool _isLevelPassed = false;
        private bool _isBoss = false;
        
        public Action<GameObject> OnLevelLoaded;
        public Action OnRecordBroken;
        public Action<int> OnCollected;
        

        protected override void Awake()
        {
            base.Awake();
            playerStats.LoadData();
            levelProgression.LastLevel = 0;
        }

        private void Start()
        {
            _isLevelPassed = false;
            levelProgression.GetLevel(levelProgression.LastLevel).LastStage = 0;
            
            
        }
        
        public void WinGame()
        {
            if (_currentLevel.LastStage == -1)
            {
                _isLevelPassed = true;
            }
            LoadLevel();
        }

        public void LoseGame()
        {
            _isLevelPassed = false;
            SaveRecordScore();
            playerStats.SaveData();
            scoreCounter.ClearScore();
            UIManager.Instance.ShowLosePanel();
        }


        private void PauseGame()
        {
            Time.timeScale = 0f;
        }

        private void UnpauseGame()
        {
            Time.timeScale = 1f;
        }


        public void UnloadLevel()
        {
            Destroy(_currentStage);
            
        }

        private void InitializeLevel(int levelIndex)
        {
            _currentLevel = levelProgression.GetLevel(levelIndex);
            UIManager.Instance.InitializeLevelUI(_currentLevel.BackgroundMaterial, _currentLevel.StageCount + 1);
            InitializeStage(_currentLevel.LastStage);
        }
        
        
        
        private void InitializeStage(int stageIndex)
        {
            print("Level: " + levelProgression.LastLevel + " Stage: " + stageIndex);
            _currentStage = Instantiate(GetCurrentStage(stageIndex,out _isBoss), circlePosition, quaternion.identity);
            if (_isBoss)
            {
                _bossConfig = _currentStage.GetComponent<BossConfig>();
                knifeSpawner.KnivesAmount =  _bossConfig.KnivesAmount;
                UIManager.Instance.InitializeStageUI(_bossConfig,
                    _currentLevel.LastStage);
                
            }
            else
            {
                _stageConfig = _currentStage.GetComponent<StageConfig>();
                knifeSpawner.KnivesAmount =  _stageConfig.KnivesAmount;
                UIManager.Instance.InitializeStageUI(_stageConfig, levelProgression.LastLevel,
                    _currentLevel.LastStage);

            }
            OnLevelLoaded?.Invoke(_currentStage);
            knifeSpawner.Initialize();
        }
        
        

        public void LoadLevel()
        {
            UnloadLevel();
            if (_isLevelPassed)
            {
                if (_isBoss){ReceiveBossSkin();}
                _isLevelPassed = false;
                levelProgression.LastLevel++;
                if (levelProgression.LastLevel == -1)
                {
                    Restart();
                    return;
                }
                
                levelProgression.GetLevel(levelProgression.LastLevel).LastStage = 0;
                InitializeLevel(levelProgression.LastLevel);
            }
            else
            {
                _currentLevel.LastStage++;
                InitializeStage(_currentLevel.LastStage);
            }
        }

        public void Restart()
        {
            UnloadLevel();
            levelProgression.LastLevel = 0;
            levelProgression.GetLevel(levelProgression.LastLevel).LastStage = 0;
            InitializeLevel(levelProgression.LastLevel);
            UIManager.Instance.HideNewRecord();

        }


        private GameObject GetCurrentStage(int stageIndex, out bool isBoss)
        {
            if (stageIndex == -1)
            { 
                isBoss = true;
                return levelProgression.BossStages.GetRandomBoss();
            }
            isBoss = false;
            return _currentLevel.GetStage(stageIndex);
        }

        private void SaveRecordScore()
        {
            if (playerStats.Record < scoreCounter.CurrentScore)
            {
                playerStats.Record = scoreCounter.CurrentScore;
                OnRecordBroken?.Invoke();
            }
        }

        public void IncreaseCollectedApples()
        {
            playerStats.CurrentApples++;
            OnCollected?.Invoke(playerStats.CurrentApples);
        }

        public int GetApplesCount()
        {
            return playerStats.CurrentApples;
        }

        private void ReceiveBossSkin()
        {
            skinsMenuController.ReceiveBossSkin(_bossConfig.CollectableSkin);
        }
        
        
        
    }
}
