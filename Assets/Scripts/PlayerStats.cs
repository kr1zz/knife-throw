using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public  class PlayerStats : MonoBehaviour
{
    private static SavedData _savedData = new SavedData();
    

    public int Record
    {
        get => _savedData.savedRecord;
        set => _savedData.savedRecord = value;
    }

    public int CurrentApples
    {
        get => _savedData.savedApples;
        set => _savedData.savedApples = value;
    }

    public int CurrentSkinID
    {
        get => _savedData.currentSkinID;
        set => _savedData.currentSkinID = value;
    }
    
    
    public List<int> UnlockedSkinsId => _savedData.unlockedSkinsId;


    public void SaveData()
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/SaveData.dat");
        binaryFormatter.Serialize(file, _savedData);
        file.Close();
    }

    public void LoadData()
    {
        if (File.Exists(Application.persistentDataPath + "/SaveData.dat"))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/SaveData.dat", FileMode.Open);
            _savedData = (SavedData) binaryFormatter.Deserialize(file);
            file.Close();
        }
        else
        {
            print("[PlayerStats] No Saved Data!");
            ResetData();
        }
    }

    private void ResetData()
    {
        _savedData.savedApples = 0;
        _savedData.savedRecord = 0;
        _savedData.unlockedSkinsId.Clear();
        _savedData.unlockedSkinsId.Add(0);
        _savedData.currentSkinID = 0;

    }

    public void SaveUnlockedSkin(int id)
    {
        UnlockedSkinsId.Add(id);
        SaveData();
    }
}

[Serializable]
class SavedData
{
    public int savedApples;
    public int savedRecord;

    public List<int> unlockedSkinsId = new List<int>();
    public int currentSkinID;

}


    



