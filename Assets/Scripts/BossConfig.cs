using System.Collections;
using System.Collections.Generic;
using Level;
using Skins;
using UnityEngine;

public class BossConfig : StageConfig
{
    [SerializeField] private string bossName;
    [SerializeField] private Color textColor;
    [SerializeField] private Skin collectableSkin;
    
    public string BossName => bossName;
    public Color TextColor => textColor;

    public Skin CollectableSkin => collectableSkin;
}
