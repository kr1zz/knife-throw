using System;
using System.Collections.Generic;
using DanielLochner.Assets.SimpleScrollSnap;
using Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;
using Image = UnityEngine.UI.Image;
using Toggle = UnityEngine.UI.Toggle;

namespace Skins
{
    public class SkinsMenuController : MonoBehaviour
    {
        [SerializeField] private GameObject skinsMenuPanel;
        [SerializeField] private GameObject skinScreenPrefab;
        [SerializeField] private GameObject knifeSlotPrefab;
        [SerializeField] private GameObject pageIconPrefab;
        [SerializeField] private SkinsCollection skinsCollection;
        [SerializeField] private PlayerStats playerStats;

        private GameObject _pagination;
        private GameObject _content;
        private GameObject _buyButton;
        private GameObject _messagePanel;
        private Transform _scrollView;
        private Transform _skinsContainer;
        private Skin _selectedSkin;
        private Image _selectedImage;
        

    
        private int _screensCount;
    
        private readonly List<GameObject> _skinsScreens = new List<GameObject>();
        private readonly List<Slot> _slots = new List<Slot>();

        public Action<Sprite> OnSkinChanged;
    
    
    
        private void Start()
        {
            Initialize();
        }


        private void Initialize()
        {
            _screensCount = skinsCollection.SkinsSets.Count;
            _scrollView = skinsMenuPanel.transform.Find("Scroll View");
            _pagination = _scrollView.Find("Pagination").gameObject;
            _content = _scrollView.Find("Viewport").Find("Content").gameObject;
            _buyButton = skinsMenuPanel.transform.Find("BuyButton").gameObject;
            _messagePanel = skinsMenuPanel.transform.Find("MessagePanel").gameObject;
            LoadUnlockedSkins();
            SpawnScreens();
        
        }


        private void SpawnScreens()
        {
            for (int i = 0; i < _screensCount; i++)
            {
                Instantiate(pageIconPrefab, _pagination.transform); 
                _skinsScreens.Add(Instantiate(skinScreenPrefab, _content.transform));
                _skinsContainer = _skinsScreens[i].transform.Find("SkinsContainer");
                _skinsScreens[i].transform.Find("TypePanel").Find("Text").GetComponent<TextMeshProUGUI>().text =
                    skinsCollection.SkinsSets[i].SkinsSetName; 
                _skinsScreens[i].transform.Find("PricePanel").Find("Text").GetComponent<TextMeshProUGUI>().text +=
                    skinsCollection.SkinsSets[i].SkinsPrice.ToString();
            
           
                for (int j = 0; j < skinsCollection.SkinsSets[i].Skins.Count; j++)
                {
                    _slots.Add(Instantiate(knifeSlotPrefab, _skinsContainer.transform).GetComponent<Slot>());
                    _slots[_slots.Count - 1].OnSkinSelected += SelectSkin;
                    _slots[_slots.Count - 1].Skin = skinsCollection.SkinsSets[i].Skins[j];
                    _slots[_slots.Count - 1].GetComponent<Toggle>().group = _content.GetComponent<ToggleGroup>();
                    
                    var image = _slots[_slots.Count - 1].transform.GetChild(1).GetComponent<Image>();
                    image.sprite = _slots[_slots.Count - 1].Skin.Sprite;
                    image.enabled = true;
                    if (skinsCollection.SkinsSets[i].Skins[j].IsUnlocked == false)
                    {
                        image.color = Color.black;
                    }
                }

            }
            SelectPrimarySlot(playerStats.CurrentSkinID);
        
        
        }

        private void ShowElement(GameObject element)
        {
            element.SetActive(true);
        }

        private void HideElement(GameObject element)
        {
            element.SetActive(false);
        }
    
    
    


        private void SelectSkin(Skin skin, Image image)
        {
            _selectedSkin = skin;
            _selectedImage = image;
            HideElement(_buyButton);
            if (_selectedSkin.IsUnlocked)
            {
                playerStats.CurrentSkinID = skin.ID;
                OnSkinChanged?.Invoke(skin.Sprite);
            }
            else if (_selectedSkin.IsPurchasable())
            {
                ShowElement(_buyButton);
            }
        }

        private void OnDisable()
        {
            for (int i = 0; i < _screensCount; i++)
            {
                if (skinsCollection.SkinsSets[i].Skins.Count < 1) return;
                for (int j = 0; j < skinsCollection.SkinsSets[i].Skins.Count; j++)
                {
                    _slots[_slots.Count - 1].OnSkinSelected -= SelectSkin;

                }
            }
        }

        private void LoadUnlockedSkins()
        {
            var skins = skinsCollection.Skins;
            var savedSkins = playerStats.UnlockedSkinsId;
            foreach (var skin in skins)
            {
                if (savedSkins == null) return;
                foreach (var savedSkin in savedSkins)
                {
                    if (skin.ID == savedSkin)
                    {
                        skin.Unlock();
                    }
                }
            }
        }

        private void UnlockSkin(Skin skin)
        {
            playerStats.CurrentSkinID = skin.ID;
            playerStats.SaveUnlockedSkin(skin.ID);
            skin.Unlock();

        }

        public void BuySkin()
        {
            if (_selectedSkin.Price <= playerStats.CurrentApples)
            {
                playerStats.CurrentApples -= _selectedSkin.Price;
                UIManager.Instance.UpdateAppleText(playerStats.CurrentApples);
                UnlockSkin(_selectedSkin);
                HideElement(_buyButton);
                VisualUnlock(_selectedImage);
                OnSkinChanged?.Invoke(_selectedSkin.Sprite);
            }
            else
            {
                ShowElement(_messagePanel);
                print("[SMC] Not enough apples" + _selectedSkin.Price);
            }
        }

        private void SelectPrimarySlot(int id)
        {
            foreach (var slot in _slots)
            {
                if (slot.slotId == id)
                {
                    var toggle = slot.GetComponent<Toggle>();
                    toggle.isOn = true;
                }
                else
                {
                    var toggle = slot.GetComponent<Toggle>();
                    toggle.isOn = false;
                    toggle.interactable = true;
                }
            }
        }
        public void ReceiveBossSkin(Skin skin)
        {
            var isExist = false;
            foreach (var slot in _slots)
            {
                if (slot.slotId == skin.ID)
                {
                    VisualUnlock(slot.transform.Find("Icon").GetComponent<Image>());
                    isExist = true;
                    print("[SMC] Skin Unlocked");
                    break;
                }
            }

            if (isExist)
            {
                UnlockSkin(skin);
            }
            else
            {
                Debug.LogWarning("[SMC] ID's don't match");
            }
        
        }

        private void VisualUnlock(Image image)
        {
            image.color = Color.white;
        }

        #region Dev
        public void ReceiveSomeApples()
        {
            playerStats.CurrentApples += 100;
            UIManager.Instance.UpdateAppleText(playerStats.CurrentApples);

        }
        

        #endregion
        
    }
}
