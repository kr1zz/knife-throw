using System;
using UnityEngine;
using UnityEngine.UI;

namespace Skins
{
    public class Slot : MonoBehaviour
    {
        public Action<Skin, Image> OnSkinSelected;

        [SerializeField] private Skin skin;
        [SerializeField] public int slotId;
        
        private Image _icon;
        private Toggle _toggle;

        public Skin Skin
        {
            get => skin;
            set
            {
                skin = value;
                slotId = skin.ID;
            }
        
        }

        

        private void Start()
        {
            _icon = transform.Find("Icon").gameObject.GetComponent<Image>();

        }

        public void Select()
        {
            OnSkinSelected?.Invoke(skin, _icon);
            
        }

        public void ChangeState()
        {
            _toggle = gameObject.GetComponent<Toggle>();
            _toggle.interactable = !_toggle.interactable;
        }
        
        
        

    
    }
}
 