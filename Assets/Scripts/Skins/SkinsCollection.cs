using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Skins;
using UnityEngine;

[CreateAssetMenu(fileName = "Skins Data")]

public class SkinsCollection : ScriptableObject
{
    [SerializeField] private List<SkinsSet> skinsSets;
    [SerializeField] private List<Skin> skins;
    [SerializeField] private bool isInitialized;
    public List<Skin> Skins
    {
        get
        {
            Initialize();
            return skins;
        }
    }
    

    public List<SkinsSet> SkinsSets => skinsSets;

    private void Initialize()
    {
        if (isInitialized) return;
        
        foreach (var skinsSet in skinsSets)
        {
            skins.AddRange(skinsSet.Skins);
            skinsSet.SetPrice();
        }
       
        CollisionsCheck();
        isInitialized = true;
    }


    private bool CollisionsCheck()
    {
        var nonUniqueId = skins.GroupBy(s => s.ID).Where(g => g.Count() > 1).Select(g => g.Key);
        if (nonUniqueId.Any())
        {
            Debug.LogWarning("[SkinsCollection] Skins identifiers not unique. " + nonUniqueId.First());
            return true;
        }
        return false;
        

    }
}
