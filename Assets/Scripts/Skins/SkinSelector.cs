using UnityEngine;

namespace Skins
{
    public class SkinSelector : MonoBehaviour
    {
        [SerializeField] private GameObject knifePrefab;
        [SerializeField] private SkinsMenuController skinsMenuController;
    
        private void OnEnable()
        {
            skinsMenuController.OnSkinChanged += ChangeSkin;

        }

        private void OnDisable()
        {
            skinsMenuController.OnSkinChanged -= ChangeSkin;
        }


        private void ChangeSkin(Sprite sprite)
        {
            knifePrefab.transform.Find("Sprite").GetComponent<SpriteRenderer>().sprite = sprite;
        }
    
    
    }
}
