using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Skins;
using UnityEngine;

[CreateAssetMenu(fileName = "Skins Set")]
public class SkinsSet : ScriptableObject
{
    [SerializeField] private List<Skin> skins;
    [SerializeField] private string skinsSetName;
    [SerializeField] private int skinsPrice;
    [SerializeField] public ReceiveType receiveType;

    public List<Skin> Skins => skins;

    public string SkinsSetName => skinsSetName;

    public int SkinsPrice => skinsPrice;


    public enum ReceiveType
    {
        Purchase,
        Unlock
    }

    private bool IsPurchasable()
    {
        return (receiveType == ReceiveType.Purchase);
    }

    public void SetPrice()
    {
        foreach (var skin in skins.Where(skin => IsPurchasable()))
        {
            skin.Price = skinsPrice;
        }
    }
    
}
