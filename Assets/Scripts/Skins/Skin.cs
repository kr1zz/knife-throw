using UnityEngine;

namespace Skins
{
    [CreateAssetMenu(fileName = "Skin")]
    public class Skin : ScriptableObject
    {
        [SerializeField] private Sprite sprite;
        [SerializeField] private int id;
        [SerializeField] private ReceiveType receiveType;
        [SerializeField] private bool isUnlocked = false;
        public int Price { get; set; }

        private enum ReceiveType
        {
            Purchase,
            Unlock
        }
    
        public bool IsPurchasable()
        {
            return (receiveType == ReceiveType.Purchase);
        }

        public void Unlock()
        {
            IsUnlocked = true;
        }


        public int ID => id;
    
        public Sprite Sprite => sprite;

        public bool IsUnlocked
        {
            get => isUnlocked;
            private set => isUnlocked = value;
        }
    }
}
