using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class KnifeSpawner : MonoBehaviour
{
    [SerializeField] private Knife knifePrefab;
    [SerializeField] private Vector2 knifeStartPosition;

    public Action<Knife> OnKnifeSpawned; 
    
    private int knivesAmount;
    private int _currentIndex = 0;
    private Transform knifePoolTransform;
    private List<Knife> _knifeClones = new List<Knife>();

    public int KnivesAmount
    {
        get => knivesAmount;
        set => knivesAmount = value;
    }
    

    private void Awake()
    {
        knifePoolTransform = gameObject.transform;
    }

    public void SpawnKnife()
    {
        if (_currentIndex < knivesAmount)
        {
            _knifeClones[_currentIndex].transform.position = knifeStartPosition;
            _knifeClones[_currentIndex].Initialize();
            OnKnifeSpawned?.Invoke(_knifeClones[_currentIndex]);
            _currentIndex++;
        }

    }
    
    private void CreatePool()
    {
        for (int i = 0; i < knivesAmount; i++)
        {
            _knifeClones.Add(Instantiate(knifePrefab, knifePoolTransform));
        }
    }

    private void Reload()
    {
        DestroyRemainingKnives();
        _currentIndex = 0;
    }

    private void DestroyRemainingKnives()
    {
        for (int i = knifePoolTransform.childCount - 1; i >= 0; i--)
        {
            Destroy(knifePoolTransform.GetChild(i).gameObject); 
        }
        _knifeClones.Clear();
    }
    
    public void Initialize()
    {
        Reload();
        CreatePool();
        SpawnKnife();
    }
    
    public bool IsLastKnife()
    {
        return (_currentIndex == knivesAmount);
    }
    
    
}
