using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyWithDelay : MonoBehaviour
{
    [SerializeField] private float delay = 1f;
    private void OnEnable()
    {
        Destroy(gameObject, delay);
    }
}
