using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Managers;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class ThrowLogic : MonoBehaviour
{
    [SerializeField] private KnifeSpawner knifeSpawner;
    [SerializeField] private ScoreCounter scoreCounter;

    private GameObject _circle;
    private Animator _animator;

    
    private Knife _currentKnife;
    private bool _isLocked;

    

    private void OnEnable()
    {
        knifeSpawner.OnKnifeSpawned += OnKnifeSpawned;
       

    }
    private void OnDisable()
    {
        knifeSpawner.OnKnifeSpawned -= OnKnifeSpawned;
        GameManager.Instance.OnLevelLoaded -= OnLevelLoaded;

        
    }

    private void FixedUpdate()
    {
        
    }

    public void Start()
    {
        GameManager.Instance.OnLevelLoaded += OnLevelLoaded;
    }
    

    public void Hit()
    {
        _currentKnife.transform.SetParent(_circle.transform);
        _currentKnife.Stick();
        _currentKnife.IsHit = true;
        scoreCounter.IncreaseCurrentScore();
        if (knifeSpawner.IsLastKnife())
        {
            _circle.GetComponent<DestroyCircle>().Destroy();
            GameManager.Instance.Invoke("WinGame", 1.5f);
        }
        else
        {
            _animator.SetTrigger("Hit");
            knifeSpawner.SpawnKnife();
            
        }

    }

    private void Miss()
    {
        
        _currentKnife.ReboundKnife();
        
    }
    
    
    public void ThrowCurrentKnife()
    {
        if (_currentKnife == null) {return;}
        if (_isLocked) {return;}
        _currentKnife.Throw();
        UIManager.Instance.DestroyKnifeIcon();
        LockThrowing();
        
    }

    


    private void LockThrowing()
    {
        _isLocked = true;
    }

    private void UnlockThrowing()
    {
        _isLocked = false;
    }

    

    private void OnKnifeSpawned(Knife knife)
    {
        if (_currentKnife != null)
        {
            _currentKnife.CollisionDetector.OnHit -= Hit;
            _currentKnife.CollisionDetector.OnMissed -= Miss;
        }
        _currentKnife = knife;
        _currentKnife.CollisionDetector.OnHit += Hit;
        _currentKnife.CollisionDetector.OnMissed += Miss;
        UnlockThrowing();
    }

    private void OnLevelLoaded(GameObject currentLevel)
    {
        _circle = currentLevel.transform.GetChild(0).gameObject;
        _animator = _circle.GetComponent<Animator>();
    
    }

    
}
