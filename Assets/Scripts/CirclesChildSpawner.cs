using System;
using System.Collections;
using System.Collections.Generic;
using Level;
using Unity.Mathematics;
using UnityEngine;

public class CirclesChildSpawner : MonoBehaviour
{
    [SerializeField] private GameObject applePrefab;
    [SerializeField] private GameObject knifePrefab;
    private float _radiusApple = 1.4f;
    private float _radiusKnife = 1.25f;
    

    private GameObject[] _apples;
    private GameObject[] _obstacles;
    private Transform _circleTransform;
    private Vector3 _forward = Vector3.zero;
    private Vector3 _backward = Vector3.back * 180f;

    private void OnEnable()
    {
        _apples = new GameObject[gameObject.GetComponent<StageConfig>().ApplesAngles.Count];
        _obstacles = new GameObject[gameObject.GetComponent<StageConfig>().ObstaclesAngles.Count];
        _circleTransform = gameObject.transform.Find("CircleVariant");    
        Spawn(gameObject.GetComponent<StageConfig>().ApplesAngles, applePrefab, _apples, _backward, _radiusApple);
        Spawn(gameObject.GetComponent<StageConfig>().ObstaclesAngles, knifePrefab, _obstacles, _forward, _radiusKnife);
        
        
    }

    private void Spawn(List<float> positionAngles, GameObject prefab, GameObject[] objects, Vector3 rotation, float radius)
    {
        
        for (int i = 0; i < positionAngles.Count; i++)
        {
            positionAngles[i] = positionAngles[i] * Mathf.Deg2Rad;
            var objPositions = new Vector3(radius * Mathf.Cos(positionAngles[i]), radius * Mathf.Sin(positionAngles[i]), 0);
            objects[i] = Instantiate(prefab, _circleTransform);
            objects[i].transform.position += objPositions;
            objects[i].transform.LookAt2D(_circleTransform);
            objects[i].transform.Rotate(rotation);
        }
    }

    
}
static class Extensions
{
    #region LookAt2D
    public static void LookAt2D(this Transform me, Vector3 target, Vector3? eye = null)
    {
        float signedAngle = Vector2.SignedAngle(eye ?? me.up, target - me.position);

        if (Mathf.Abs(signedAngle) >= 1e-3f)
        {
            var angles = me.eulerAngles;
            angles.z += signedAngle;
            me.eulerAngles = angles;
        }
    }
    public static void LookAt2D(this Transform me, Transform target, Vector3? eye = null)
    {
        me.LookAt2D(target.position, eye);
    }
    public static void LookAt2D(this Transform me, GameObject target, Vector3? eye = null)
    {
        me.LookAt2D(target.transform.position, eye);
    }
    #endregion
}